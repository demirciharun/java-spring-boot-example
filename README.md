# Spring Boot Hello World

A spring boot enabled hello world application

- From executable jar file
```
mvn clean install
java -jar target/helloworld-0.0.1-SNAPSHOT.jar
```
